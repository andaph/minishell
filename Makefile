# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alouis <alouis@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/12/10 14:05:03 by alouis            #+#    #+#              #
#    Updated: 2021/02/05 17:45:31 by alouis           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re _make _clean _fclean re

NAME= minishell

CC= clang

CFLAGS= -Wall -Wextra -Werror

LFLAGS= -L./libft -lft

SRCS_DIR= srcs

OBJS_DIR= objs

SRCS= main.c \
	change_directory.c \
	manage_echo.c \
	change_directory_2.c \
	change_directory_3.c \
	manage_env.c \
	manage_env_2.c \
	redirect_cmd.c \
	redirect_cmd2.c \
	manage_pwd.c \
	manage_exec.c \
	manage_exec_2.c \
	manage_pipe.c \
	manage_pipe2.c \
	init_and_free.c \
	export.c \
	unset.c \
	parsing.c \
	exit.c \
	error.c \
	copy_quotes.c \
	split_cli.c \
	cleaning.c \
	backslash.c \
	check_quotes.c \
	expansion.c \
	redirection.c \
	manage_redirection.c \
	manage_redirection2.c \
	utils.c \
	sort_env.c \
	remove_quotes.c \
	print_for_debug.c


OBJS = $(addprefix $(OBJS_DIR)/,$(SRCS:.c=.o))

LIBS = libft/libft.a

INC= -I./includes

all:
	make _make
	make $(NAME)

$(NAME): $(OBJS) $(LIBS)
	$(CC) ${CFLAGS} $(OBJS) -o $(NAME) $(LFLAGS)
# $(CC) ${CFLAGS}  $(LFLAGS) $(OBJS) -o $(NAME)
# compilation line for MacOS

$(OBJS_DIR)/%.o: $(SRCS_DIR)/%.c
	mkdir -p $(OBJS_DIR)
	${CC} ${CFLAGS} $(INC) -c $< -o $@

clean: 
	make _clean
	rm -rf $(OBJS_DIR)

fclean: clean
	make _fclean
	rm -rf $(NAME)

re: fclean all
	make _re

_make:
	make -C libft/				#flag -C to execute command make in folder

_clean:
	make -C libft clean

_fclean:
	make -C libft fclean

_re:
	make -C libft re
