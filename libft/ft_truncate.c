/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_truncate.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/11 16:17:09 by alouis            #+#    #+#             */
/*   Updated: 2020/12/11 17:41:08 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_truncate(char *str, size_t start, size_t end)
{
	int	i;
	int	y;
	int	len;

	i = 0;
	y = start;
	len = ft_strlen(str) - start - end;
	if (start >= ft_strlen(str) - end || end >= ft_strlen(str) - start ||
		len <= 0)
		return (NULL);
	while (str[start] && start <= (unsigned int)len + y - 1)
	{
		str[i] = str[start];
		i++;
		start++;
	}
	while ((unsigned int)i <= ft_strlen(str))
	{
		str[i] = '\0';
		i++;
	}
	return (str);
}
