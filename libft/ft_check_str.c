/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_str.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/19 16:07:20 by alouis            #+#    #+#             */
/*   Updated: 2020/03/07 14:09:02 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_search(char c, char *hay)
{
	int i;

	i = 0;
	while (hay[i])
	{
		if (hay[i] == c)
			return (0);
		i++;
	}
	return (-1);
}

int	ft_check_str(char *needle, char *hay)
{
	int j;

	j = 0;
	if (!needle || !hay)
		return (-1);
	while (needle[j])
	{
		if (ft_search(needle[j], hay) == -1)
			return (-1);
		j++;
	}
	return (0);
}
