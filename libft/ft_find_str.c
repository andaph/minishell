/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_str.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/16 18:19:59 by alouis            #+#    #+#             */
/*   Updated: 2020/12/16 18:20:02 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_find_str(char *needle, char *hay)
{
	int	i;
	int	j;

	i = 0;
	if (!needle || !hay)
		return (-1);
	while (hay[i])
	{
		j = 0;
		if (hay[i] == needle[j])
		{
			while (hay[i] && hay[i] == needle[j])
			{
				i++;
				j++;
			}
			if (needle[j] == '\0')
				return (0);
		}
		else
			i++;
	}
	return (-1);
}
