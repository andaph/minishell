/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/07 11:03:30 by alouis            #+#    #+#             */
/*   Updated: 2019/11/16 14:21:43 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	size_t	len_dst;
	size_t	len_src;
	char	*d;
	char	*s;

	d = dst;
	s = (char *)src;
	len_dst = 0;
	len_src = 0;
	if (!dstsize && !dst)
		return (ft_strlen(src));
	while (s[len_src])
		len_src++;
	while (*d)
	{
		d++;
		if ((unsigned long)(d - dst) >= dstsize)
			return (len_src + dstsize);
	}
	len_dst = d - dst;
	while (*s && (unsigned long)(d - dst) < dstsize - 1)
		*d++ = *s++;
	*d = '\0';
	return (len_src + len_dst);
}
