/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   first_last.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 12:45:39 by alouis            #+#    #+#             */
/*   Updated: 2020/03/07 16:33:52 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	first_last(char *str, char c)
{
	size_t len;

	if (!str)
		return (-1);
	len = ft_strlen(str);
	if (str[0] == c && str[len - 1] == c)
		return (0);
	return (-1);
}

int	fst_lst(char *str, char w, char *blanks)
{
	size_t len;

	if (!str || !blanks)
		return (-1);
	len = ft_strlen(str) - 1;
	while (len != 0)
	{
		if (!ft_search(str[len], blanks))
			len--;
		else if (str[len] == w)
			break ;
		else
			return (-1);
	}
	while (*str)
	{
		if (!ft_search(*str, blanks))
			str++;
		else if (*str == w)
			return (0);
		else
			return (-1);
	}
	return (-1);
}
