/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_addstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/31 11:46:19 by alouis            #+#    #+#             */
/*   Updated: 2021/02/05 17:27:02 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	free_char_tab(char **tab)
{
	int	i;

	i = 0;
	while (tab[i] != 0)
	{
		free(tab[i]);
		i++;
	}
	free(tab);
}

char	**ft_stradd_front(char **tab, char *str)
{
	char	**tmp;
	int		i;
	int		j;

	i = 0;
	j = 0;
	if (!(tmp = (char **)malloc(sizeof(char *) * (ft_tablen(tab) + 2))))
		return (NULL);
	tmp[i] = ft_strdup(str);
	i++;
	while (tab[j])
	{
		tmp[i] = ft_strdup(tab[j]);
		i++;
		j++;
	}
	tmp[i] = 0;
	free_char_tab(tab);
	return (tmp);
}

char	**ft_stradd_back(char **tab, char *str)
{
	char	**tmp;
	int		i;

	i = 0;
	if (!(tmp = (char **)malloc(sizeof(char *) * (ft_tablen(tab) + 2))))
		return (NULL);
	while (tab[i])
	{
		tmp[i] = ft_strdup(tab[i]);
		i++;
	}
	tmp[i] = ft_strdup(str);
	i++;
	tmp[i] = 0;
	free_char_tab(tab);
	return (tmp);
}

char	**replace_strintab(char **tab, char *str, int pos)
{
	int		i;
	char	**new;

	i = -1;
	if (pos < 0 || pos >= ft_tablen(tab) || !str)
		return (tab);
	if (!(new = (char **)malloc(sizeof(char *) * (ft_tablen(tab) + 1))))
		return (NULL);
	while (++i != pos)
		new[i] = ft_strdup(tab[i]);
	new[i] = ft_strdup(str);
	while (tab[++i])
		new[i] = ft_strdup(tab[i]);
	new[i] = 0;
	free_char_tab(tab);
	return (new);
}
