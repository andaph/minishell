/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pos_last_item.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/11 16:57:32 by alouis            #+#    #+#             */
/*   Updated: 2020/12/11 16:59:59 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned int	pos_last_item(char *str, char c)
{
	unsigned int	i;
	unsigned int	y;

	i = ft_strlen(str) - 1;
	y = 0;
	if (!str)
		return (0);
	while (str[i])
	{
		if (str[i] == c && str[i + 1])
			return (y);
		i--;
		y++;
	}
	return (0);
}
