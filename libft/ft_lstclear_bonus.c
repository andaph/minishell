/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear_bonus.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/15 14:05:40 by alouis            #+#    #+#             */
/*   Updated: 2019/11/15 14:06:08 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void *))
{
	t_list *tmp;
	t_list *tsl;

	tsl = *lst;
	while (tsl != 0)
	{
		(*del)(tsl->content);
		tmp = tsl->next;
		free(tsl);
		tsl = tmp;
	}
	*lst = NULL;
}
