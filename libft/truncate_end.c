/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   truncate_end.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/11 16:03:07 by alouis            #+#    #+#             */
/*   Updated: 2020/12/11 16:09:17 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*trunc_end(char *str, size_t len)
{
	unsigned int i;

	i = ft_strlen(str) - len + 1;
	if (len >= ft_strlen(str) || i >= ft_strlen(str))
		return (NULL);
	while (i < ft_strlen(str))
	{
		str[i] = '\0';
		i++;
	}
	return (str);
}
