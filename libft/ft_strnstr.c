/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/07 13:38:01 by alouis            #+#    #+#             */
/*   Updated: 2020/03/03 16:19:30 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	if (!needle[i])
		return ((char *)haystack);
	if (!haystack && !len)
		return (NULL);
	while (haystack[i] && i < len && len)
	{
		j = 0;
		if (needle[j] == haystack[i])
		{
			while (needle[j] == haystack[i + j] &&
					(i + j <= len))
			{
				j++;
				if (needle[j] == 0)
					return ((char *)haystack + i);
			}
		}
		i++;
	}
	return (NULL);
}
