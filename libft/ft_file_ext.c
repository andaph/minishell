/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_file_ext.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/22 15:40:31 by alouis            #+#    #+#             */
/*   Updated: 2020/03/06 11:03:53 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_file_ext(char *str, char *ext)
{
	char *cmp;

	cmp = ft_strnstr(str, ext, ft_strlen(str));
	if (cmp && !ft_strncmp(cmp, ext, ft_strlen(cmp)))
		return (0);
	return (-1);
}
