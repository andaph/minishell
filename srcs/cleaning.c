/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cleaning.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/31 14:26:34 by alouis            #+#    #+#             */
/*   Updated: 2021/02/05 16:17:27 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	copy_after_backslash(char **tmp, char **new, int *i)
{
	(*tmp)++;
	if (*tmp)
		copy_char(tmp, *new, i);
}

void	copy_after_simple_quote(char **tmp, char **new, int *i)
{
	if (!ft_strncmp(*tmp, "\'>\'", 3) || !ft_strncmp(*tmp, "\'<\'", 3) ||
		!ft_strncmp(*tmp, "\'>>\'", 4))
	{
		copy_char(tmp, *new, i);
		while (**tmp != '\'')
			copy_char(tmp, *new, i);
		copy_char(tmp, *new, i);
	}
	else
	{
		(*tmp)++;
		while (**tmp && **tmp != '\'')
			copy_char(tmp, *new, i);
		if (*tmp)
			(*tmp)++;
	}
}

void	copy_after_double_quote(char **tmp, char **new, int *i)
{
	if (!ft_strncmp(*tmp, "\">\"", 3) || !ft_strncmp(*tmp, "\"<\"", 3) ||
		!ft_strncmp(*tmp, "\">>\"", 4))
	{
		copy_char(tmp, *new, i);
		while (**tmp != '\"')
			copy_char(tmp, *new, i);
		copy_char(tmp, *new, i);
	}
	else
	{
		(*tmp)++;
		while (**tmp && **tmp != '\"')
		{
			if (**tmp == '\\')
				(*tmp)++;
			copy_char(tmp, *new, i);
		}
		if (*tmp)
			(*tmp)++;
	}
}

int		remove_quotes(char **str)
{
	char	*tmp;
	char	*new;
	int		i;

	i = 0;
	tmp = *str;
	if (!(new = (char *)malloc(sizeof(char) * (ft_strlen(*str) + 1))))
		return (-1);
	while (*tmp)
	{
		if (*tmp == '\\')
			copy_after_backslash(&tmp, &new, &i);
		else if (*tmp == '\'')
			copy_after_simple_quote(&tmp, &new, &i);
		else if (*tmp == '\"')
			copy_after_double_quote(&tmp, &new, &i);
		else
			copy_char(&tmp, new, &i);
	}
	new[i] = '\0';
	free(*str);
	*str = new;
	return (0);
}

int		ft_cleaning(t_msh *msh)
{
	char	*tmp;
	int		i;

	i = 0;
	tmp = msh->cmd->cmd;
	if (ft_check_quotes(tmp) || ft_expansion(msh, tmp) ||
			remove_quotes(&msh->exp))
		return (-1);
	free(msh->cmd->cmd);
	msh->cmd->cmd = ft_strdup(msh->exp);
	free(msh->exp);
	if (!(msh->exp = ft_calloc(sizeof(char), 1)))
		return (-1);
	while (msh->cmd->arg[i])
	{
		tmp = msh->cmd->arg[i];
		if (ft_check_quotes(tmp) || ft_expansion(msh, tmp) ||
				remove_quotes(&msh->exp) || replace_in_tab(msh, i))
			return (-1);
		free(msh->exp);
		if (!(msh->exp = ft_calloc(sizeof(char), 1)))
			return (-1);
		i++;
	}
	return (0);
}
