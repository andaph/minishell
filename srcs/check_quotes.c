/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_quotes.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/11 09:23:13 by alouis            #+#    #+#             */
/*   Updated: 2021/02/01 11:29:04 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	find_snd_quote(char **tmp, char quote)
{
	(*tmp)++;
	while (**tmp)
	{
		if (quote == '\"' && **tmp == '\\' && !quote_is_escaped(*tmp, '\"'))
		{
			while (**tmp && **tmp != '\"')
				(*tmp)++;
			if (*tmp)
				(*tmp)++;
		}
		if (quote == '\"' && **tmp == '\\' && quote_is_escaped(*tmp, '\"'))
		{
			while (**tmp && **tmp != '\"')
				(*tmp)++;
			return (0);
		}
		else if (**tmp && quote == '\"' && **tmp == '\"')
			return (0);
		else if (quote == '\'' && **tmp == '\'')
			return (0);
		if (**tmp)
			(*tmp)++;
	}
	return (-1);
}

int	ft_check_quotes(char *tmp)
{
	int		esc;
	char	*check;

	check = tmp;
	while (*check)
	{
		esc = 0;
		while (*check == '\\')
		{
			check++;
			esc++;
		}
		if ((esc % 2) != 0 && *check)
			check++;
		if (*check && ((*check == '\"' && find_snd_quote(&check, '\"')) ||
				(*check == '\'' && find_snd_quote(&check, '\''))))
		{
			ft_putstr_fd("-bash: missing matching quote\n", 1);
			return (-1);
		}
		if (*check)
			check++;
	}
	return (0);
}

int	ft_is_literal(char *cmd, int i)
{
	if (!cmd || i < 0 || i > (int)ft_strlen(cmd))
		return (-1);
	while (i > 0)
	{
		i--;
		if (cmd[i] == '\'' && !is_escaped(cmd, i))
		{
			if (even_unesc_of(cmd, '\'', i) && even_unesc_of(cmd, '\"', i))
				return (0);
			else
				return (1);
		}
		if (cmd[i] == '\"' && !is_escaped(cmd, i))
		{
			if (even_unesc_of(cmd, '\"', i) && even_unesc_of(cmd, '\'', i))
				return (1);
			else
				return (0);
		}
	}
	return (0);
}
