/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirection.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 16:59:20 by alouis            #+#    #+#             */
/*   Updated: 2021/02/06 00:04:02 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		replace_cmd(t_cmds *cmd, int *i)
{
	while (cmd->arg[*i])
	{
		if (!ft_strcmp(cmd->arg[*i], ">") || !ft_strcmp(cmd->arg[*i], ">>") ||
			!ft_strcmp(cmd->arg[*i], "<"))
			redirection_arg(cmd, i);
		else if (cmd->arg[*i])
		{
			free(cmd->cmd);
			cmd->cmd = ft_strdup(cmd->arg[*i]);
			remove_arg(cmd, *i);
			return (0);
		}
	}
	free(cmd->cmd);
	cmd->cmd = ft_strdup("\0");
	return (0);
}

int		remove_arg(t_cmds *cmd, int i)
{
	char	**new;
	int		j;

	j = 0;
	if (!((new = (char **)malloc(sizeof(char *) * (ft_tablen(cmd->arg))))))
		return (-1);
	while (j < i)
	{
		new[j] = ft_strdup(cmd->arg[j]);
		j++;
	}
	i = i + 1;
	while (cmd->arg[i])
		new[j++] = ft_strdup(cmd->arg[i++]);
	new[j] = 0;
	free_char_tab(cmd->arg);
	cmd->arg = new;
	return (0);
}

int		redirection_cmd(t_cmds *cmd, int *i)
{
	if (!cmd->arg[*i])
		return (-1);
	if (!ft_strcmp(cmd->cmd, ">") &&
		init_newfd(&cmd->out, TRUNC, cmd->arg[*i]))
		return (-1);
	else if (!ft_strcmp(cmd->cmd, ">>") &&
		init_newfd(&cmd->out, APPEND, cmd->arg[*i]))
		return (-1);
	else if (!ft_strcmp(cmd->cmd, "<") &&
		init_newfd(&cmd->in, RDWR, cmd->arg[*i]))
		return (-1);
	if (remove_arg(cmd, *i) || replace_cmd(cmd, i))
		return (-1);
	return (0);
}

int		redirection_arg(t_cmds *cmd, int *i)
{
	if (!cmd->arg[*i + 1])
		return (-1);
	if (!ft_strcmp(cmd->arg[*i], ">") &&
		init_newfd(&cmd->out, TRUNC, cmd->arg[*i + 1]))
		return (-1);
	else if (!ft_strcmp(cmd->arg[*i], ">>") &&
		init_newfd(&cmd->out, APPEND, cmd->arg[*i + 1]))
		return (-1);
	else if (!ft_strcmp(cmd->arg[*i], "<") &&
		init_newfd(&cmd->in, RDWR, cmd->arg[*i + 1]))
		return (-1);
	if (remove_arg(cmd, *i) || remove_arg(cmd, *i))
		return (-1);
	return (0);
}

int		redirection(t_cmds *cmd)
{
	int	i;

	i = 0;
	if (ft_strcmp(cmd->cmd, "\0") && !ft_check_str(cmd->cmd, "><") &&
		redirection_cmd(cmd, &i))
		return (-1);
	while (cmd->arg[i])
	{
		if (ft_strcmp(cmd->arg[i], "\0") && !ft_check_str(cmd->arg[i], "><")
			&& redirection_arg(cmd, &i))
			return (-1);
		else if (cmd->arg[i] && ft_strcmp(cmd->arg[i], "<") &&
				ft_strcmp(cmd->arg[i], ">>") && ft_strcmp(cmd->arg[i], ">"))
			i++;
	}
	if (cmd->in == 0)
		init_newfd(&cmd->in, STDIN, NULL);
	if (cmd->out == 0)
		init_newfd(&cmd->out, STDOUT, NULL);
	if (remove_quotes_redirection(cmd))
		return (-1);
	return (0);
}
