/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unset.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <dgoudet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 09:41:49 by dgoudet           #+#    #+#             */
/*   Updated: 2021/01/29 18:36:39 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	recreate_env(char **temp, t_msh *msh)
{
	int	i;

	i = 0;
	while (temp[i])
	{
		msh->env[i] = ft_strdup(temp[i]);
		i++;
	}
	msh->env[i] = 0;
}

void	create_temp(char ***temp, t_msh *msh, int i)
{
	int	j;

	j = 0;
	while (j < i)
	{
		(*temp)[j] = ft_strdup(msh->env[j]);
		j++;
	}
	j++;
	while (msh->env[j])
	{
		(*temp)[i] = ft_strdup(msh->env[j]);
		i++;
		j++;
	}
	(*temp)[i] = 0;
}

int		ft_unset2(t_msh *msh, int i, char **comp)
{
	char	**temp;

	if (!(temp = malloc(sizeof(char *) * (ft_tablen(msh->env)))))
	{
		free(*comp);
		return (1);
	}
	create_temp(&temp, msh, i);
	free_char_tab(msh->env);
	if (!(msh->env = malloc(sizeof(char *) * (ft_tablen(temp) + 1))))
	{
		free(*comp);
		return (1);
	}
	recreate_env(temp, msh);
	free_char_tab(temp);
	return (0);
}

void	ft_unset(t_msh *msh)
{
	int		i;
	int		j;
	int		k;
	char	*comp;

	k = 0;
	while (msh->cmd->arg[k])
	{
		i = 0;
		j = 0;
		comp = ft_strjoin(msh->cmd->arg[k], "=");
		while (msh->env[i] && ft_strncmp(comp, msh->env[i],
					ft_strlen(msh->cmd->arg[k]) + 1) != 0)
			i++;
		if (msh->env[i] == 0)
		{
			free(comp);
			error_127(msh);
			return ;
		}
		if (ft_unset2(msh, i, &comp) == 1)
			return ;
		k++;
		free(comp);
	}
}
