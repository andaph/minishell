/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   remove_quotes.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 17:42:08 by alouis            #+#    #+#             */
/*   Updated: 2021/02/05 19:09:00 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_copy_after_simple_quote(char **tmp, char **new, int *i)
{
	(*tmp)++;
	while (**tmp && **tmp != '\'')
		copy_char(tmp, *new, i);
	if (*tmp)
		(*tmp)++;
}

void	ft_copy_after_double_quote(char **tmp, char **new, int *i)
{
	(*tmp)++;
	while (**tmp && **tmp != '\"')
	{
		if (**tmp == '\\')
			(*tmp)++;
		copy_char(tmp, *new, i);
	}
	if (*tmp)
		(*tmp)++;
}

int		remove_all_quotes(char **str)
{
	char	*tmp;
	char	*new;
	int		i;

	i = 0;
	tmp = *str;
	if (!(new = (char *)malloc(sizeof(char) * (ft_strlen(*str) + 1))))
		return (-1);
	while (*tmp)
	{
		if (*tmp == '\\')
			copy_after_backslash(&tmp, &new, &i);
		else if (*tmp == '\'')
			ft_copy_after_simple_quote(&tmp, &new, &i);
		else if (*tmp == '\"')
			ft_copy_after_double_quote(&tmp, &new, &i);
		else
			copy_char(&tmp, new, &i);
	}
	new[i] = '\0';
	free(*str);
	*str = new;
	return (0);
}

int		remove_quotes_redirection(t_cmds *cmd)
{
	int		i;
	char	**tmp;

	i = 0;
	while (cmd->arg[i])
	{
		tmp = &cmd->arg[i];
		if (!ft_find_str("\">\"", *tmp) || !ft_find_str("\"<\"", *tmp) ||
		!ft_find_str("\">>\"", *tmp) || !ft_find_str("\'>\'", *tmp) ||
		!ft_find_str("\'<\'", *tmp) || !ft_find_str("\'>>\'", *tmp))
		{
			if (remove_all_quotes(tmp))
				return (-1);
			cmd->arg = replace_strintab(cmd->arg, *tmp, i);
			if (cmd->arg == NULL)
				return (-1);
		}
		i++;
	}
	return (0);
}
