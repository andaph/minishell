/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirect_cmd2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/31 14:18:27 by alouis            #+#    #+#             */
/*   Updated: 2021/02/06 00:02:08 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	manage_builtin(t_msh *msh, int *ret)
{
	char	pwd[1000];

	*ret = 0;
	if (!ft_strcmp("exit", msh->cmd->cmd))
		ft_exit(msh, NULL, "exit\n");
	else if (!ft_strcmp("pwd", msh->cmd->cmd))
		return (print_cwd(msh));
	else if (msh->cmd->cmd[0] == '/')
		return (change_directory(msh->cmd->cmd, msh));
	else if (!ft_strcmp("cd", msh->cmd->cmd))
	{
		if (msh->cmd->arg[0])
			change_directory(msh->cmd->arg[0], msh);
		else
			change_directory("\0", msh);
		ft_setenv("PWD", getcwd(pwd, 1000), msh);
		return ;
	}
	else if (!ft_strcmp("echo", msh->cmd->cmd))
		return (echo(msh));
	else if (!ft_strcmp("env", msh->cmd->cmd) && msh->cmd->arg[0])
		return (ft_putstr_fd("env: Too many arguments\n", 1));
	else if (!ft_strcmp("env", msh->cmd->cmd))
		return (write_env(msh));
	*ret = -1;
}

void	manage_builtin2(t_msh *msh, int *ret)
{
	*ret = 0;
	if (!ft_strcmp("export", msh->cmd->cmd))
	{
		if (msh->cmd->arg[0])
			return (ft_export(msh));
	}
	else if (!ft_strcmp("unset", msh->cmd->cmd))
	{
		if (msh->cmd->arg[0])
			return (ft_unset(msh));
		else
		{
			msh->err = 1;
			return (ft_putstr_fd("unset: not enough arguments\n", 1));
		}
	}
	else if (!ft_strcmp("./", msh->cmd->cmd))
		return (ft_putstr_fd("-bash: ./: permission denied\n", 1));
	*ret = -1;
}

void	redirect_cmd(t_msh *msh)
{
	msh->err = 0;
	if (ft_cmdsize(msh->cmd) > 1)
	{
		msh->cmd = msh->cmd_tmp;
		manage_pipe(msh);
	}
	else if (!ft_strcmp(msh->cmd->cmd, "\0"))
		return ;
	else
		redirect_cmd2(msh);
}

void	redirect_cmd2(t_msh *msh)
{
	int ret;

	ret = 0;
	manage_builtin(msh, &ret);
	if (ret == -1)
		manage_builtin2(msh, &ret);
	if (ret == -1)
	{
		if (msh->cmd->out->fd != 1 || nbr_of_fd(msh->cmd->out) > 1)
			ret = redirect_and_exec(msh->cmd->cmd, &msh->cmd->arg, \
				msh->cmd->out, msh);
		else
			ret = manage_exec(msh->cmd->cmd, &msh->cmd->arg, msh);
	}
	if (ret == -1)
		error_127(msh);
}
