/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expansion.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 17:01:21 by alouis            #+#    #+#             */
/*   Updated: 2021/01/14 17:01:23 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		replace_in_tab(t_msh *msh, int i)
{
	int		j;
	char	**exp_tmp;

	j = 0;
	if (!(exp_tmp = (char **)malloc(sizeof(char *) * \
					(ft_tablen(msh->cmd->arg) + 1))))
		return (-1);
	while (j != i)
	{
		exp_tmp[j] = ft_strdup(msh->cmd->arg[j]);
		j++;
	}
	exp_tmp[j] = ft_strdup(msh->exp);
	j++;
	while (msh->cmd->arg[j])
	{
		exp_tmp[j] = ft_strdup(msh->cmd->arg[j]);
		j++;
	}
	exp_tmp[j] = 0;
	free_char_tab(msh->cmd->arg);
	msh->cmd->arg = exp_tmp;
	return (0);
}

int		replace_with_value(t_msh *msh, char *arg, int *i, int j)
{
	char	*tmp;
	char	*tmp_exp;
	char	*value;

	tmp_exp = ft_strdup(msh->exp);
	tmp = ft_substr(arg, j, (*i - j));
	free(msh->exp);
	msh->exp = (!ft_strcmp(tmp_exp, "\0")) ? ft_strjoin(tmp, tmp_exp) :
		ft_strjoin(tmp_exp, tmp);
	free(tmp);
	free(tmp_exp);
	if (arg[*i + 1] == '?')
	{
		value = ft_itoa(msh->err);
		(*i) = (*i) + 2;
	}
	else if ((value = ft_get_value(msh->env, arg, i)) == NULL)
		return (-1);
	tmp_exp = ft_strdup(msh->exp);
	free(msh->exp);
	msh->exp = ft_strjoin(tmp_exp, value);
	free(value);
	free(tmp_exp);
	return (0);
}

char	*ft_get_value(char **env, char *str, int *i)
{
	int		j;
	int		brace;
	char	*varname;
	char	*value;

	j = 0;
	brace = 0;
	if (!str || (*i) < 0 || (*i) > (int)ft_strlen(str) ||
	!(varname = (char *)malloc(sizeof(char) * (ft_strlen(str) + 1))))
		return (NULL);
	if (str[++(*i)] == '{')
	{
		brace = 1;
		(*i)++;
	}
	while (str[(*i)] && (ft_isalnum(str[(*i)]) || str[(*i)] == '_'))
		varname[j++] = str[(*i)++];
	varname[j] = '\0';
	if (brace == 1 && str[(*i)] != '}')
		return (put_error(varname));
	if (brace == 1)
		(*i)++;
	value = ft_getenv(varname, env);
	free(varname);
	return (value);
}

void	get_end_arg(t_msh *msh, char *arg, int j, int len)
{
	char	*tmp;
	char	*tmp_exp;

	tmp = NULL;
	tmp = ft_substr(arg, j, len);
	if (tmp != NULL)
	{
		tmp_exp = ft_strdup(msh->exp);
		free(msh->exp);
		msh->exp = ft_strjoin(tmp_exp, tmp);
		free(tmp);
		free(tmp_exp);
	}
}

int		ft_expansion(t_msh *msh, char *arg)
{
	int	i;
	int	j;
	int	len;

	i = 0;
	j = 0;
	len = 0;
	while (arg[i])
	{
		if (arg[i] == '$' && arg[i + 1] && !is_escaped(arg, i) &&
		!ft_is_literal(arg, i) && arg[i + 1] != '\"' &&
		arg[i + 1] != '\'' && !ft_isdigit(arg[i + 1]))
		{
			if (replace_with_value(msh, arg, &i, j))
				return (-1);
			j = i;
		}
		if (arg[i])
			i++;
		len = i - j;
	}
	get_end_arg(msh, arg, j, len);
	return (0);
}
