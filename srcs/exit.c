/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/21 12:26:37 by alouis            #+#    #+#             */
/*   Updated: 2021/01/29 20:05:30 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_exit(t_msh *msh, void *other, char *msg)
{
	if (msh->err != 0)
		errno = msh->err;
	if (msg)
		ft_putstr_fd(msg, 1);
	free_char_tab(msh->env);
	if (msh->tmp != NULL)
		ft_lstclear(&msh->tmp, free);
	else if (msh->cli != NULL)
		ft_lstclear(&msh->cli, free);
	if (msh->cmd)
		free_cmds(&msh->cmd);
	free_char_tab(msh->cmd->arg);
	free(msh->cmd);
	if (msh->exp)
	{
		free(msh->exp);
		msh->exp = NULL;
	}
	free(msh);
	if (other != NULL)
		free(other);
	exit(errno);
}

char	*put_error(char *varname)
{
	ft_putstr_fd("-bash: missing matching {\n", 1);
	free(varname);
	return (NULL);
}
