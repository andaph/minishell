/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_cli.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 16:59:39 by alouis            #+#    #+#             */
/*   Updated: 2021/01/14 16:59:43 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*create_maillon(t_msh *msh, char *content, size_t len)
{
	char	*tmp;
	t_list	*new;

	new = ft_lstnew(content);
	ft_lstadd_back(&msh->cli, new);
	if (!(tmp = ft_calloc(sizeof(char), len)))
		ft_exit(msh, NULL, "Calloc error\n");
	return (tmp);
}

char	*copy_specials(t_msh *msh, char **ln, char *tmp, char *spec)
{
	int i;

	i = 0;
	if (*tmp)
		tmp = create_maillon(msh, tmp, ft_strlen(*ln) + 1);
	while (!ft_search(**ln, spec))
		copy_char(ln, tmp, &i);
	tmp = create_maillon(msh, tmp, ft_strlen(*ln) + 1);
	return (tmp);
}

void	copy_words(char **ln, char *tmp, int i)
{
	i = (int)ft_strlen(tmp);
	if (**ln == '\"')
	{
		copy_char(ln, tmp, &i);
		if (**ln)
			copy_quotes(ln, tmp, &i, '\"');
	}
	else if (**ln == '\'')
	{
		copy_char(ln, tmp, &i);
		while (**ln && **ln != '\'')
			copy_char(ln, tmp, &i);
		if (**ln)
			copy_char(ln, tmp, &i);
	}
	else if (**ln == '\\')
	{
		while (**ln && **ln == '\\')
			copy_char(ln, tmp, &i);
		if (**ln && is_escaped(tmp, (int)ft_strlen(tmp)))
			copy_char(ln, tmp, &i);
	}
	else
		copy_char(ln, tmp, &i);
}

t_list	*split_cmdline(t_msh *msh, char *ln)
{
	char	*tmp;
	int		i;

	i = 0;
	if (!(tmp = ft_calloc(sizeof(char), ft_strlen(ln) + 1)))
		return (NULL);
	while (*ln)
	{
		if (*ln == '\\')
			copy_escaped(&ln, tmp);
		else if (!ft_search(*ln, " \t\n"))
			tmp = copy_specials(msh, &ln, tmp, " \t\n");
		else if (!ft_search(*ln, ";|><"))
			tmp = copy_specials(msh, &ln, tmp, ";|><");
		else
			copy_words(&ln, tmp, i);
	}
	if (*tmp)
		tmp = create_maillon(msh, tmp, ft_strlen(tmp) + 1);
	free(tmp);
	return (msh->cli);
}
