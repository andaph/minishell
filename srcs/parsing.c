/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/23 18:54:44 by alouis            #+#    #+#             */
/*   Updated: 2021/01/29 16:38:16 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		end_of_cmd(char *str, int pipe, int *eop)
{
	if (!ft_check_str(str, ";"))
	{
		if (ft_strcmp(str, ";"))
		{
			ft_putstr_fd("-bash: syntax error near unexpected token ';'\n", 1);
			return (-1);
		}
		if (pipe != 0)
		{
			*eop = *eop + 1;
			return (2);
		}
		return (1);
	}
	else if (!ft_check_str(str, "|"))
	{
		if (ft_strcmp(str, "|"))
		{
			ft_putstr_fd("-bash: syntax error near unexpected token '|'\n", 1);
			return (-1);
		}
		return (2);
	}
	ft_putstr_fd("-bash: syntax error near unexpected token\n", 1);
	return (-1);
}

int		stock_cmd(t_cmds *cmd, char *str)
{
	if (!ft_check_str(str, "><") && (ft_strcmp(str, "<") &&
				ft_strcmp(str, ">") && ft_strcmp(str, ">>")))
	{
		ft_putstr_fd("-bash: syntax error near unexpected bracket\n", 1);
		return (-1);
	}
	if (!cmd->cmd)
		cmd->cmd = ft_strdup(str);
	else
		cmd->arg = ft_stradd_back(cmd->arg, str);
	return (0);
}

int		split_cmds(t_cmds *cmd, char *str, int pipe, int *eop)
{
	if (!ft_check_str(str, " \t\n"))
		return (0);
	else if (!ft_check_str(str, "|") || !ft_check_str(str, ";"))
		return (end_of_cmd(str, pipe, eop));
	else
		return (stock_cmd(cmd, str));
}

void	parsing(t_msh *msh, char *ln)
{
	msh->cmd_tmp = msh->cmd;
	msh->pipe = 0;
	msh->eop = 0;
	free_init_cli(msh, ln);
	while (msh->cli != 0)
	{
		msh->cmd->end = split_cmds(msh->cmd, msh->cli->content, msh->pipe, \
		&msh->eop);
		if (msh->cmd->end == -1 || redirect_out(msh))
		{
			return_error(msh);
			return ;
		}
		msh->cli = msh->cli->next;
	}
	if (redirect_last_cmd(msh))
		return_error(msh);
}
