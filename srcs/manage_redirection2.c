/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_redirection2.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/30 07:58:21 by dgoudet           #+#    #+#             */
/*   Updated: 2021/02/04 20:55:31 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		exec2(char *path, char **arg, t_msh *msh, int fd)
{
	pid_t	pid;
	int		status;
	int		exit_status;

	msh->cmd->in->fd = open_file(msh->cmd->in);
	if ((pid = fork()) == -1)
		return (-1);
	else if (pid == 0)
	{
		dup2(msh->cmd->in->fd, 0);
		dup2(fd, 1);
		execve(path, arg, msh->env);
		ft_exit(msh, NULL, NULL);
	}
	else
	{
		waitpid(0, &status, 0);
		exit_status = WEXITSTATUS(status);
		set_err(exit_status, msh);
	}
	if (msh->cmd->in->fd != 0)
		close(msh->cmd->in->fd);
	return (0);
}

int		exec_file2(char *cmd, char ***arg, int fd, t_msh *msh)
{
	int ret;

	ret = 0;
	if (check_directory(cmd) == 1)
	{
		if (ft_strncmp(cmd, "/bin/", 5) == 0)
		{
			add_cmd_to_arg(cmd, arg);
			ret = exec2(cmd, *arg, msh, fd);
		}
		else if (cmd[0] == '.')
			ret = exec2(cmd, *arg, msh, fd);
		return (ret);
	}
	ft_putstr_fd("-bash: No such file or directory\n", 1);
	return (-1);
}

int		redirect_and_exec2(char *cmd, char ***arg, int fd, t_msh *msh)
{
	char	**path_tab;
	char	*path;
	int		ret;

	ret = 0;
	if (cmd[0] == '/' || cmd[0] == '.')
		return (exec_file2(cmd, arg, fd, msh));
	if ((path_tab = create_path_tab(msh->env)) == NULL)
	{
		error_127(msh);
		return (0);
	}
	if (((path = find_path(cmd, path_tab)) == NULL)
			|| (add_cmd_to_arg(cmd, arg) == 1))
	{
		free_char_tab(path_tab);
		free(path);
		return (-1);
	}
	else
		ret = exec2(path, *arg, msh, fd);
	free(path);
	free_char_tab(path_tab);
	return (ret);
}

void	increment(t_cmds **tmp, t_cmds **tmp2)
{
	*tmp2 = (*tmp2)->next;
	*tmp = *tmp2;
}

int		send_fd_and_commands(t_msh *msh)
{
	t_cmds	*tmp;
	t_cmds	*tmp2;
	t_fd	*out;

	tmp = msh->cmd;
	tmp2 = msh->cmd;
	while (tmp2->cmd != 0 && tmp->cmd != 0)
	{
		if (nbr_of_fd(tmp2->out) > 1 || tmp2->out->fd != 1 ||
				tmp2->next->cmd == 0)
		{
			out = tmp2->out;
			while (out != NULL)
			{
				out->fd = open_file(out);
				exec_commands(tmp, tmp2->next, out->fd, msh);
				close(out->fd);
				out = out->next;
			}
			increment(&tmp, &tmp2);
		}
		else
			tmp2 = tmp2->next;
	}
	return (0);
}
