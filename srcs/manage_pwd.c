/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_pwd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/11 11:09:35 by alouis            #+#    #+#             */
/*   Updated: 2021/02/05 09:59:38 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	print_cwd(t_msh *msh)
{
	char	dir[1000];
	char	*ret;
	t_fd	*tmp;

	tmp = msh->cmd->out;
	ret = getcwd(dir, 1000);
	if (ret == NULL)
		exit(errno);
	else
	{
		while (tmp != NULL)
		{
			tmp->fd = open_file(tmp);
			ft_putstr_fd(dir, tmp->fd);
			ft_putstr_fd("\n", tmp->fd);
			if (tmp->fd != 1)
				close(tmp->fd);
			tmp = tmp->next;
		}
	}
}
