/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   change_directory_2.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <dgoudet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 08:36:44 by dgoudet           #+#    #+#             */
/*   Updated: 2021/02/05 09:22:17 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	change_directory_3(char *buf, t_msh *msh)
{
	t_fd *tmp;

	tmp = msh->cmd->out;
	if (check_directory(buf) == 1)
	{
		if (ft_strncmp(buf, "/bin/", 5) == 0)
		{
			add_cmd_to_arg(buf, &msh->cmd->arg);
			loop(buf, msh->cmd->arg, tmp, msh);
		}
		else
		{
			msh->err = 127;
			ft_putstr_fd("-bash: No such file or directory\n", 1);
		}
		return ;
	}
	else
		chdir(buf);
}

int		up_and_create_path(char *buf, int *i, char **path)
{
	char	*temp;
	int		j;

	if (buf[*i] == '\0')
	{
		chdir(*path);
		free(*path);
		return (1);
	}
	while (buf[*i] == '.')
		(*i)++;
	if (buf[*i] == '/')
		(*i)++;
	j = ft_strlen(*path);
	if (j == 0)
		j = 1;
	if ((*path)[j] == '/' && j > 1)
		j--;
	while (j > 1 && (*path)[j] != '/')
		j--;
	temp = ft_strdup(*path);
	free(*path);
	*path = ft_substr(temp, 0, j);
	free(temp);
	return (0);
}

void	check_if_more_points(char *buf, int *i)
{
	int	j;

	if ((buf[*i] >= 'a' && buf[*i] <= 'z')
			|| (buf[*i] >= 'A' || buf[*i] <= 'Z'))
	{
		j = *i;
		while ((buf[j] >= 'a' && buf[j] <= 'z')
				|| (buf[j] >= 'A' && buf[j] <= 'Z'))
			j++;
		if (buf[j] == '/')
		{
			j++;
			if (buf[j] == '.')
				j = j + 2;
			if (buf[j] == '/')
				j++;
			*i = j;
		}
	}
}

void	recreate_path(char *buf, int i, char **path)
{
	char	*temp;
	char	*temp2;

	temp = ft_strjoin(*path, "/");
	temp2 = ft_substr(buf, i, ft_strlen(buf) - i);
	free(*path);
	*path = ft_strjoin(temp, temp2);
	free(temp);
	free(temp2);
}

void	get_and_chdir(char *buf)
{
	int		i;
	char	*path;
	char	dir[1000];

	getcwd(dir, 1000);
	if (ft_strncmp("./", buf, 2) == 0)
		i = 2;
	else
		i = 0;
	path = ft_strdup(dir);
	while (buf[i] == '.')
	{
		while (buf[i + 1] == '/')
			i = i + 2;
		if (buf[i + 1] == '\0')
			i++;
		if (buf[i] == '.' || buf[i] == '/' || buf[i] == '\0')
			if (up_and_create_path(buf, &i, &path) == 1)
				return ;
		check_if_more_points(buf, &i);
	}
	if (buf[i] != '\0')
		recreate_path(buf, i, &path);
	chdir(path);
	free(path);
}
