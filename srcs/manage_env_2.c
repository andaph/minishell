/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_env_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 09:07:53 by dgoudet           #+#    #+#             */
/*   Updated: 2021/02/05 13:02:44 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	store_env(t_msh *msh)
{
	extern char	**environ;
	int			i;

	if (!(msh->env = malloc(sizeof(char *) * (ft_tablen(environ) + 1))))
		return ;
	i = 0;
	while (environ[i])
	{
		msh->env[i] = ft_strdup(environ[i]);
		i++;
	}
	msh->env[i] = 0;
}

void	write_env(t_msh *msh)
{
	int		i;
	int		j;
	t_fd	*tmp;

	tmp = msh->cmd->out;
	while (tmp != 0)
	{
		i = 0;
		tmp->fd = open_file(tmp);
		while (msh->env[i])
		{
			j = 0;
			while (msh->env[i][j])
			{
				write(tmp->fd, &msh->env[i][j], 1);
				j++;
			}
			write(tmp->fd, "\n", 1);
			i++;
		}
		if (tmp->fd != 1)
			close(tmp->fd);
		tmp = tmp->next;
	}
}
