/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/17 10:36:51 by dgoudet           #+#    #+#             */
/*   Updated: 2021/02/04 10:03:18 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	sighandler(int signum)
{
	(void)signum;
	if (signum == SIGINT)
	{
		if (g_sig == 0)
			write(1, "\nPrompt : ", 10);
		else
			write(1, "\n", 1);
	}
	if (signum == SIGQUIT && g_quit == 1)
	{
		write(1, "\nPrompt : ", 10);
		g_sig = 1;
		g_quit = 0;
	}
}

void	init_msh(t_msh *msh)
{
	store_env(msh);
	msh->cli = NULL;
	msh->tmp = NULL;
	msh->err = 0;
	if (!(msh->exp = ft_calloc(sizeof(char), 1)))
		ft_exit(msh, NULL, "Calloc error\n");
	if (!(msh->cmd = init_cmds()))
		ft_exit(msh, NULL, "Malloc error for struct t_cmds *cmd\n");
	g_sig = 0;
	g_quit = 0;
}

int		main(int argc, char **argv)
{
	t_msh	*msh;
	char	*buf;

	buf = NULL;
	(void)argv;
	if (argc != 1)
		return (1);
	signal(SIGQUIT, sighandler);
	if (!(msh = malloc(sizeof(t_msh))))
		return (1);
	init_msh(msh);
	while (1)
	{
		signal(SIGINT, &sighandler);
		write(1, "Prompt : ", 9);
		g_quit = 0;
		if ((get_next_line(0, &buf)) > 0)
			parsing(msh, buf);
		else
			ft_exit(msh, buf, NULL);
		g_sig = 0;
	}
	return (0);
}
