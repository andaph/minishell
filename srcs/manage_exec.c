/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_exec.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 09:14:54 by dgoudet           #+#    #+#             */
/*   Updated: 2021/02/04 12:56:11 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	**create_path_tab(char **env)
{
	char	*path_value;
	char	**path_tab;

	if ((path_value = ft_getenv("PATH", env)) == NULL)
		return (NULL);
	path_tab = ft_split(path_value, ':');
	free(path_value);
	return (path_tab);
}

char	*find_path(char *cmd, char **path_tab)
{
	int		i;
	int		ret;
	char	*path;
	char	*temp;

	i = 0;
	ret = -1;
	temp = ft_strjoin("/", cmd);
	while (path_tab[i])
	{
		path = ft_strjoin(path_tab[i], temp);
		if ((ret = open(path, O_RDONLY)) >= 0)
		{
			free(temp);
			close(ret);
			return (path);
		}
		free(path);
		path = NULL;
		i++;
	}
	free(temp);
	return (path);
}

int		exec_file(char *path, char **arg, t_msh *msh)
{
	pid_t	cpid;
	int		status;
	int		exit_status;

	cpid = fork();
	g_sig = 1;
	msh->cmd->in->fd = open_file(msh->cmd->in);
	if (cpid == 0)
	{
		dup2(msh->cmd->in->fd, 0);
		execve(path, arg, msh->env);
		g_quit = 1;
		ft_exit(msh, NULL, NULL);
	}
	else
	{
		waitpid(0, &status, 0);
		exit_status = WEXITSTATUS(status);
		set_err(exit_status, msh);
	}
	if (msh->cmd->in->fd != 0)
		close(msh->cmd->in->fd);
	return (0);
}

int		add_cmd_to_arg(char *cmd, char ***arg)
{
	char	**temp;
	int		i;

	i = 0;
	temp = ft_tabdup(*arg);
	free_char_tab(*arg);
	if (!(*arg = malloc(sizeof(char *) * (ft_tablen(temp) + 2))))
	{
		free(temp);
		return (1);
	}
	*arg[0] = ft_strdup(cmd);
	while (temp[i])
	{
		(*arg)[i + 1] = ft_strdup(temp[i]);
		i++;
	}
	(*arg)[i + 1] = 0;
	free_char_tab(temp);
	return (0);
}

int		manage_exec(char *cmd, char ***arg, t_msh *msh)
{
	char	**path_tab;
	char	*path;
	int		ret;

	ret = 0;
	if (cmd[0] == '.' || cmd[0] == '/')
		ret = manage_exec_2(cmd, arg, msh);
	else
	{
		if ((path_tab = create_path_tab(msh->env)) == NULL)
			return (-1);
		if (((path = find_path(cmd, path_tab)) == NULL)
				|| (add_cmd_to_arg(cmd, arg) == 1))
		{
			free_char_tab(path_tab);
			free(path);
			return (-1);
		}
		else
			ret = exec_file(path, *arg, msh);
		free(path);
		free_char_tab(path_tab);
	}
	return (ret);
}
