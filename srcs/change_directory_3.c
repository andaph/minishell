/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   change_directory_3.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <dgoudet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 09:21:27 by dgoudet           #+#    #+#             */
/*   Updated: 2021/02/05 09:22:01 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	loop(char *buf, char **arg, t_fd *tmp, t_msh *msh)
{
	while (tmp != 0)
	{
		tmp->fd = open_file(tmp);
		exec_binary(buf, arg, tmp->fd, msh);
		if (tmp->fd != 1)
			close(tmp->fd);
		tmp = tmp->next;
	}
}
