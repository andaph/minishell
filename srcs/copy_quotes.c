/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copy_quotes.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/23 17:32:41 by alouis            #+#    #+#             */
/*   Updated: 2021/02/01 11:45:34 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		quote_is_escaped(char *str, char c)
{
	int	i;
	int	esc;

	i = 0;
	esc = 0;
	if (!str)
		return (-1);
	if (c == '\\' || c == '\'' || c == '\"')
	{
		while (str[i] && str[i] == '\\')
		{
			i++;
			esc++;
		}
		if (str[i] == c && (esc % 2) != 0)
			return (0);
		return (-1);
	}
	return (-1);
}

void	copy_char(char **ln, char *tmp, int *i)
{
	if (**ln)
	{
		tmp[(*i)++] = **ln;
		(*ln)++;
	}
}

void	copy_quotes(char **ln, char *tmp, int *i, char quote)
{
	while (**ln && **ln != quote)
	{
		if (quote == '\"' && **ln == '\\' && quote_is_escaped(*ln, '\"'))
		{
			while (**ln && **ln != '\"')
				copy_char(ln, tmp, i);
			if (**ln)
				copy_char(ln, tmp, i);
			return ;
		}
		else if (**ln == '\\')
		{
			while (**ln == '\\')
				copy_char(ln, tmp, i);
			if (**ln)
				copy_char(ln, tmp, i);
		}
		else
			copy_char(ln, tmp, i);
	}
	if (**ln)
		copy_char(ln, tmp, i);
}
