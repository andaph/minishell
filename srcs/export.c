/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   export.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <dgoudet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/15 14:22:17 by dgoudet           #+#    #+#             */
/*   Updated: 2021/01/21 09:01:42 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		create_varname(char *buf, char **name, int *i)
{
	*i = 0;
	while (buf[*i] && buf[*i] != '=')
		(*i)++;
	if (buf[*i] == '\0')
		return (1);
	*name = ft_substr(buf, 0, *i);
	return (0);
}

void	create_varvalue(char *buf, char **value, int i)
{
	int	j;

	j = i;
	while (buf[j])
		j++;
	*value = ft_substr(buf, i, j - i);
}

void	ft_export(t_msh *msh)
{
	int		i;
	int		j;
	int		ret;
	char	*name;
	char	*value;

	j = 0;
	while (msh->cmd->arg[j])
	{
		if ((ret = create_varname(msh->cmd->arg[j], &name, &i)) != 0)
			j++;
		else
		{
			i++;
			create_varvalue(msh->cmd->arg[j], &value, i);
			ft_setenv(name, value, msh);
			free(name);
			free(value);
			j++;
		}
	}
}
