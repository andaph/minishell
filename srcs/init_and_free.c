/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_and_free.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 17:00:09 by alouis            #+#    #+#             */
/*   Updated: 2021/02/04 17:40:27 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		init_newfd(t_fd **fd, t_mode md, char *file)
{
	t_fd	*new;
	t_fd	*tmp;

	if (!(new = (t_fd *)malloc(sizeof(t_fd))))
		return (-1);
	new->md = md;
	if (md == STDOUT)
		new->fd = 1;
	else
		new->fd = 0;
	if (file)
		new->fl = ft_strdup(file);
	else
		new->fl = NULL;
	new->next = 0;
	if (*fd == 0)
		*fd = new;
	else
	{
		tmp = *fd;
		while (tmp->next != 0)
			tmp = tmp->next;
		tmp->next = new;
	}
	return (0);
}

void	free_fd(t_fd **fd)
{
	t_fd	*tmp;
	t_fd	*df;

	df = *fd;
	while (df != 0)
	{
		if (df->fl)
			free(df->fl);
		tmp = df->next;
		free(df);
		df = tmp;
	}
	*fd = 0;
}

t_cmds	*init_cmds(void)
{
	t_cmds	*new;

	if (!(new = (t_cmds *)malloc(sizeof(t_cmds))))
		return (NULL);
	new->cmd = 0;
	new->end = 0;
	new->in = 0;
	new->out = 0;
	if (!(new->arg = (char **)malloc(sizeof(char *))))
		return (NULL);
	new->arg[0] = 0;
	new->next = NULL;
	return (new);
}

void	free_cmds(t_cmds **cmd)
{
	t_cmds	*tmp;
	t_cmds	*dmc;

	dmc = *cmd;
	while (dmc != 0)
	{
		if (dmc->in != 0)
			free_fd(&dmc->in);
		if (dmc->out != 0)
			free_fd(&dmc->out);
		free_char_tab(dmc->arg);
		if (dmc->cmd)
			free(dmc->cmd);
		tmp = dmc->next;
		free(dmc);
		dmc = tmp;
	}
	free(dmc);
	*cmd = init_cmds();
}

void	free_init_cli(t_msh *msh, char *ln)
{
	if (msh->tmp != NULL)
	{
		ft_lstclear(&msh->tmp, free);
		msh->cli = NULL;
		msh->tmp = NULL;
	}
	msh->cli = split_cmdline(msh, ln);
	msh->tmp = msh->cli;
	free(ln);
}
