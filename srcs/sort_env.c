/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_env.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/01 14:57:23 by alouis            #+#    #+#             */
/*   Updated: 2021/02/01 15:01:09 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	sort_env(t_msh *msh)
{
	free(msh->cmd->cmd);
	msh->cmd->cmd = ft_strdup("env");
	msh->cmd->end = 2;
	msh->cmd_tmp = msh->cmd;
	ft_pipeadd_back(&msh->cmd);
	while (msh->cmd->next != 0)
		msh->cmd = msh->cmd->next;
	msh->cmd->cmd = ft_strdup("sort");
	msh->cmd->end = 2;
	init_newfd(&msh->cmd->in, STDIN, NULL);
	init_newfd(&msh->cmd->out, STDOUT, NULL);
	ft_pipeadd_back(&msh->cmd);
	msh->cmd = msh->cmd_tmp;
}

void	sort_env_inlist(t_msh *msh)
{
	t_cmds *new;

	free(msh->cmd->cmd);
	msh->cmd->cmd = ft_strdup("env");
	new = init_cmds();
	new->cmd = ft_strdup("sort");
	init_newfd(&new->in, STDIN, NULL);
	init_newfd(&new->out, STDOUT, NULL);
	new->end = 2;
	new->next = msh->cmd->next;
	msh->cmd->next = new;
	msh->cmd = msh->cmd->next;
}
