/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_echo.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/11 12:59:37 by alouis            #+#    #+#             */
/*   Updated: 2021/02/05 12:43:56 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	print_args(char **arg, int fd)
{
	int	i;
	int	n;

	i = 0;
	n = 0;
	if (arg[i])
	{
		if (!ft_strcmp(arg[i], "-n"))
		{
			i++;
			n = 1;
		}
		while (arg[i])
		{
			ft_putstr_fd(arg[i], fd);
			if (arg[i + 1])
				ft_putstr_fd(" ", fd);
			i++;
		}
		if (!n)
			ft_putstr_fd("\n", fd);
	}
}

void	print_in_files(t_msh *msh)
{
	t_fd	*fd;

	fd = msh->cmd->out;
	while (fd != 0)
	{
		fd->fd = open_file(fd);
		if (msh->cmd->arg[0])
			print_args(msh->cmd->arg, fd->fd);
		else
			ft_putstr_fd("\n", fd->fd);
		close(fd->fd);
		fd = fd->next;
	}
}

void	echo(t_msh *msh)
{
	if (msh->cmd->out->md != STDOUT)
		print_in_files(msh);
	else if (msh->cmd->arg[0])
		print_args(msh->cmd->arg, msh->cmd->out->fd);
	else
		ft_putstr_fd("\n", msh->cmd->out->fd);
}
