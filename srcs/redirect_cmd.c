/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirect_cmd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/31 14:18:27 by alouis            #+#    #+#             */
/*   Updated: 2021/02/06 00:01:28 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	redirect_pipe(t_msh *msh)
{
	msh->pipe = msh->pipe + 1;
	if (!msh->cmd->cmd)
	{
		ft_putstr_fd("-bash: syntax error near token `|'\n", 1);
		return (-1);
	}
	if (ft_cleaning(msh))
		return (return_error(msh));
	if (redirection(msh->cmd))
		return (return_error(msh));
	ft_pipeadd_back(&msh->cmd);
	if (!ft_strcmp("export", msh->cmd->cmd) && !msh->cmd->arg[0])
		sort_env_inlist(msh);
	return (0);
}

int	redirect_last_cmd(t_msh *msh)
{
	if (msh->pipe)
	{
		if (redirect_pipe(msh))
			return (-1);
		msh->cmd->end = 2;
		redirect_cmd(msh);
		msh->cmd = msh->cmd_tmp;
		free_cmds(&msh->cmd);
		msh->eop = msh->eop - msh->eop;
		msh->pipe = msh->pipe - msh->pipe;
	}
	else if (msh->cmd->cmd)
	{
		if (ft_cleaning(msh))
			return (return_error(msh));
		if (redirection(msh->cmd))
			return (return_error(msh));
		if (!ft_strcmp("export", msh->cmd->cmd) && !msh->cmd->arg[0])
			sort_env(msh);
		redirect_cmd(msh);
		free_cmds(&msh->cmd);
		msh->cmd_tmp = msh->cmd;
	}
	return (0);
}

int	redirect_out(t_msh *msh)
{
	if (msh->cmd->end == 1 || msh->eop == 1)
		return (redirect_last_cmd(msh));
	else if (msh->cmd->end == 2)
	{
		if (redirect_pipe(msh))
			return (-1);
		msh->cmd = msh->cmd->next;
	}
	return (0);
}
