/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_redirection.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/23 13:43:15 by dgoudet           #+#    #+#             */
/*   Updated: 2021/02/05 19:07:50 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	exec(char *path, char **arg, t_msh *msh, int fd)
{
	pid_t	pid;
	int		status;
	int		exit_status;

	g_sig = 1;
	msh->cmd->in->fd = open_file(msh->cmd->in);
	if ((pid = fork()) == -1)
		return (-1);
	else if (pid == 0)
	{
		dup2(msh->cmd->in->fd, 0);
		dup2(fd, 1);
		execve(path, arg, msh->env);
		ft_exit(msh, NULL, NULL);
	}
	else
	{
		waitpid(0, &status, 0);
		exit_status = WEXITSTATUS(status);
		set_err(exit_status, msh);
	}
	if (msh->cmd->in->fd != 0)
		close(msh->cmd->in->fd);
	return (0);
}

int	redirect_and_exec1(char *cmd, char ***arg, t_fd *tmp, t_msh *msh)
{
	int	ret;

	ret = 0;
	while (tmp != NULL)
	{
		tmp->fd = open_file(tmp);
		ret = exec_file2(cmd, arg, tmp->fd, msh);
		close(tmp->fd);
		tmp = tmp->next;
	}
	return (ret);
}

int	redirect_and_exec_step2(char *path, char **arg, t_msh *msh, t_fd *tmp)
{
	int ret;

	ret = 0;
	while (tmp != NULL)
	{
		tmp->fd = open_file(tmp);
		ret = exec(path, arg, msh, tmp->fd);
		close(tmp->fd);
		tmp = tmp->next;
	}
	return (ret);
}

int	redirect_and_exec(char *cmd, char ***arg, t_fd *tmp, t_msh *msh)
{
	char	**path_tab;
	char	*path;
	int		ret;

	ret = 0;
	if (cmd[0] == '/' || cmd[0] == '.')
		return (redirect_and_exec1(cmd, arg, tmp, msh));
	if ((path_tab = create_path_tab(msh->env)) == NULL)
		return (-1);
	if (((path = find_path(cmd, path_tab)) == NULL)
			|| (add_cmd_to_arg(cmd, arg) == 1))
	{
		free_char_tab(path_tab);
		free(path);
		return (-1);
	}
	else
		ret = redirect_and_exec_step2(path, *arg, msh, tmp);
	free(path);
	free_char_tab(path_tab);
	return (ret);
}
