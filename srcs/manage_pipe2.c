/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_pipe2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 18:45:44 by dgoudet           #+#    #+#             */
/*   Updated: 2021/02/06 08:58:06 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	close_fd_in(int fd)
{
	if (fd != 0)
		close(fd);
}

int		create_pipe_and_fork(int (*pipefd)[2], pid_t *pid, t_cmds *start)
{
	int	fd;

	pipe(*pipefd);
	*pid = fork();
	fd = open_file(start->in);
	return (fd);
}
