/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_pipe.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 09:36:12 by dgoudet           #+#    #+#             */
/*   Updated: 2021/02/06 08:58:22 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	exec_parent(int (*pipefd)[2], int *fd_in, t_cmds **tmp, t_msh *msh)
{
	int status;
	int exit_status;

	waitpid(0, &status, 0);
	exit_status = WEXITSTATUS(status);
	set_err(exit_status, msh);
	close((*pipefd)[1]);
	*fd_in = (*pipefd)[0];
	*tmp = (*tmp)->next;
}

void	configure_child_input(int (*pipefd)[2], int *fd_in, int fdtest)
{
	close((*pipefd)[0]);
	if (fdtest != 0)
		dup2(fdtest, 0);
	else
		dup2(*fd_in, 0);
}

void	configure_child_output(int (*pipefd)[2], int *fd_out, t_cmds *start)
{
	*fd_out = start->out->fd;
	dup2((*pipefd)[1], 1);
}

int		exec_commands(t_cmds *start, t_cmds *end, int fd, t_msh *msh)
{
	int		pipefd[2];
	pid_t	pid;
	int		fd_in;
	int		fd_out;
	int		fdtest;

	fd_in = 0;
	while (start != end)
	{
		fdtest = create_pipe_and_fork(&pipefd, &pid, start);
		if (pid == 0)
		{
			configure_child_input(&pipefd, &fd_in, fdtest);
			if (start->next != end)
				configure_child_output(&pipefd, &fd_out, start);
			else
				fd_out = fd;
			redirect_and_exec2(start->cmd, &start->arg, fd_out, msh);
			ft_exit(msh, NULL, NULL);
		}
		else
			exec_parent(&pipefd, &fd_in, &start, msh);
		close_fd_in(fdtest);
	}
	return (0);
}

int		manage_pipe(t_msh *msh)
{
	int		status;
	int		exit_status;
	pid_t	pid;

	print_struct(msh->cmd, "HELLO");
	g_sig = 1;
	if ((pid = fork()) == -1)
		return (-1);
	else if (pid == 0)
	{
		send_fd_and_commands(msh);
		ft_exit(msh, NULL, NULL);
	}
	else
	{
		waitpid(0, &status, 0);
		exit_status = WEXITSTATUS(status);
		set_err(exit_status, msh);
	}
	return (0);
}
