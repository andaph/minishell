/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_env.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <dgoudet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 09:03:07 by dgoudet           #+#    #+#             */
/*   Updated: 2021/02/04 19:03:53 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

size_t	varname_len(char *env)
{
	size_t	i;

	i = 0;
	if (!env)
		return (0);
	while (env[i] != '=')
		i++;
	return (i);
}

char	*ft_getenv(char *varname, char **env)
{
	int		i;
	int		j;
	char	*value;
	char	*comp;

	i = 0;
	j = 0;
	comp = ft_strjoin(varname, "=");
	while (env[i] && ft_strncmp(comp, env[i], varname_len(env[i]) + 1) != 0)
		i++;
	if (env[i] && ft_strncmp(comp, env[i], varname_len(env[i]) + 1) == 0)
	{
		while (env[i][j] != '=')
			j++;
		j++;
		value = ft_substr(env[i], j, ft_strlen(env[i]) - j);
	}
	else
		value = NULL;
	free(comp);
	return (value);
}

char	*create_env_var(char *name, char *value)
{
	char	*str1;
	char	*str2;

	str1 = ft_strjoin(name, "=");
	str2 = ft_strjoin(str1, value);
	free(str1);
	return (str2);
}

void	enlarge_env(t_msh *msh, char *varname, char *value)
{
	char	**temp;
	int		i;

	if (!(temp = malloc(sizeof(char *) * (ft_tablen(msh->env) + 1))))
		return ;
	i = 0;
	while (msh->env[i])
	{
		temp[i] = ft_strdup(msh->env[i]);
		i++;
	}
	temp[i] = 0;
	free_char_tab(msh->env);
	i = 0;
	if (!(msh->env = malloc(sizeof(char *) * (ft_tablen(temp) + 2))))
		return ;
	while (temp[i])
	{
		msh->env[i] = ft_strdup(temp[i]);
		i++;
	}
	msh->env[i] = create_env_var(varname, value);
	msh->env[i + 1] = 0;
	free_char_tab(temp);
}

void	ft_setenv(char *varname, char *value, t_msh *msh)
{
	int		i;
	int		j;
	char	*comp;

	i = 0;
	j = 0;
	comp = ft_strjoin(varname, "=");
	while (msh->env[i]
			&& ft_strncmp(comp, msh->env[i], ft_strlen(varname) + 1) != 0)
		i++;
	free(comp);
	if (msh->env[i] == '\0')
		enlarge_env(msh, varname, value);
	else
	{
		free(msh->env[i]);
		msh->env[i] = create_env_var(varname, value);
	}
}
