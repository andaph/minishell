/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   change_directory.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <dgoudet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 08:29:11 by dgoudet           #+#    #+#             */
/*   Updated: 2021/01/29 16:26:59 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		check_directory(char *dir)
{
	DIR	*directory;

	if ((directory = opendir(dir)) == NULL)
		return (1);
	else
	{
		closedir(directory);
		return (0);
	}
}

void	up_and_chdir(char *buf, char *dir)
{
	int		i;
	int		j;
	char	*temp;
	char	*path;

	i = 1;
	path = ft_strdup(dir);
	j = ft_strlen(path) - 1;
	while (buf[i] == '.' && j > 0)
	{
		j = ft_strlen(path) - 1;
		if (path[j] == '/')
			j--;
		while (j > 0 && path[j] != '/' && path[j])
			j--;
		temp = ft_strdup(path);
		free(path);
		if (j == 0)
			j = 1;
		path = ft_substr(temp, 0, j);
		free(temp);
		i++;
	}
	chdir(path);
	free(path);
}

int		get_cd_type(char *buf)
{
	if (ft_strncmp("...", buf, 3) == 0)
		return (4);
	else if (ft_strncmp(".", buf, 1) == 0)
		return (1);
	else if (ft_strcmp("\0", buf) == 0 || ft_strcmp("~", buf) == 0)
		return (2);
	else if (ft_strcmp("-", buf) == 0)
		return (5);
	else if ((buf[0] >= 'a' && buf[0] <= 'z')
			|| (buf[0] >= 'A' && buf[0] <= 'Z'))
		return (3);
	else if (buf[0] == '/')
		return (6);
	return (10);
}

void	change_directory_2(int ret, char *buf, t_msh *msh)
{
	char	*path;
	char	dir[1000];

	getcwd(dir, 1000);
	if (ret == 4)
		up_and_chdir(buf, dir);
	else if (ret == 5)
	{
		path = ft_getenv("OLDPWD", msh->env);
		chdir(path);
		write(1, path, ft_strlen(path));
		write(1, "\n", 1);
		free(path);
	}
	else if (ret == 6)
		change_directory_3(buf, msh);
	ft_setenv("OLDPWD", dir, msh);
}

void	change_directory(char *buf, t_msh *msh)
{
	int		ret;
	char	*path;

	ret = get_cd_type(buf);
	if (ret == 1 || ret == 3)
	{
		if (check_directory(buf) == 1)
		{
			ft_putstr_fd("Incorrect path or directory\n", 1);
			msh->err = 1;
			return ;
		}
		if (ret == 1)
			get_and_chdir(buf);
		else
			chdir(buf);
	}
	else if (ret == 2)
	{
		path = ft_getenv("HOME", msh->env);
		chdir(path);
		free(path);
	}
	change_directory_2(ret, buf, msh);
}
