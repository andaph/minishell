/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <dgoudet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/15 12:11:45 by dgoudet           #+#    #+#             */
/*   Updated: 2021/01/21 09:57:37 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		return_error(t_msh *msh)
{
	if (msh->pipe)
		msh->cmd = msh->cmd_tmp;
	free_cmds(&msh->cmd);
	msh->err = 1;
	return (-1);
}

void	error_127(t_msh *msh)
{
	msh->err = 127;
	ft_putstr_fd("-bash: ", 1);
	ft_putstr_fd(msh->cmd->cmd, 1);
	ft_putstr_fd(": command not found\n", 1);
}

void	set_err(int exit_status, t_msh *msh)
{
	if (exit_status == 2)
		msh->err = 127;
	else
		msh->err = exit_status;
}
