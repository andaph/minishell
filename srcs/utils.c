/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/21 12:26:37 by alouis            #+#    #+#             */
/*   Updated: 2021/02/05 13:03:28 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_pipeadd_back(t_cmds **cmd)
{
	t_cmds	*tmp;
	t_cmds	*new;

	tmp = *cmd;
	if (tmp->cmd == 0)
		return ;
	else
	{
		new = init_cmds();
		while (tmp->next != 0)
			tmp = tmp->next;
		tmp->next = new;
	}
}

int		ft_cmdsize(t_cmds *cmd)
{
	int	i;

	i = 0;
	while (cmd != 0)
	{
		i++;
		cmd = cmd->next;
	}
	return (i);
}

int		nbr_of_fd(t_fd *fd)
{
	int	nb;

	nb = 0;
	while (fd != 0)
	{
		nb++;
		fd = fd->next;
	}
	return (nb);
}

int		open_file(t_fd *rdc)
{
	int fd;

	fd = -1;
	if (rdc->md == TRUNC)
		fd = open(rdc->fl, O_CREAT | O_RDWR | O_TRUNC, 0666);
	else if (rdc->md == APPEND)
		fd = open(rdc->fl, O_CREAT | O_RDWR | O_APPEND, 0666);
	else if (rdc->md == RDWR)
		fd = open(rdc->fl, O_RDWR);
	else if (rdc->md == STDIN)
		fd = 0;
	else if (rdc->md == STDOUT)
		fd = 1;
	return (fd);
}
