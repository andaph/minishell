/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   backslash.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 19:25:54 by alouis            #+#    #+#             */
/*   Updated: 2021/01/10 19:25:56 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		is_escaping(char *str)
{
	int	esc;

	esc = 0;
	if (!str || *str != '\\')
		return (0);
	while (*str && *str == '\\')
	{
		str++;
		esc++;
	}
	return (esc % 2);
}

int		is_escaped(char *str, int i)
{
	int	esc;

	esc = 0;
	if (!str || i > (int)ft_strlen(str))
		return (0);
	i--;
	while (i >= 0 && str[i] == '\\')
	{
		esc++;
		i--;
	}
	return (esc % 2);
}

int		even_unesc_of(char *str, char c, int i)
{
	int	nb;

	nb = 0;
	if (!str || i < 0 || i > (int)ft_strlen(str))
		return (-1);
	while (i >= 0)
	{
		if (str[i] == c && is_escaped(str, i))
		{
			i--;
			while (str[i] == '\\')
				i--;
		}
		else if (str[i] == c)
		{
			nb++;
			i--;
		}
		else
			i--;
	}
	return (nb % 2);
}

void	copy_escaped(char **ln, char *tmp)
{
	int	i;

	i = (int)ft_strlen(tmp);
	if (**ln == '\\')
	{
		copy_char(ln, tmp, &i);
		if (**ln)
			copy_char(ln, tmp, &i);
	}
}
