/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_exec_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <dgoudet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 09:32:46 by dgoudet           #+#    #+#             */
/*   Updated: 2021/02/05 19:06:38 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	exec_binary(char *buf, char **arg, int fd_out, t_msh *msh)
{
	pid_t	cpid;
	int		status;
	int		exit_status;

	msh->cmd->in->fd = open_file(msh->cmd->in);
	g_sig = 1;
	cpid = fork();
	if (cpid == 0)
	{
		dup2(msh->cmd->in->fd, 0);
		dup2(fd_out, 1);
		execve(buf, arg, msh->env);
		exit(errno);
	}
	else
	{
		waitpid(0, &status, 0);
		exit_status = WEXITSTATUS(status);
		set_err(exit_status, msh);
	}
	if (msh->cmd->in->fd != 0)
		close(msh->cmd->in->fd);
}

int		manage_exec_2(char *cmd, char ***arg, t_msh *msh)
{
	char	*path;
	int		ret;

	path = ft_strdup(cmd);
	ret = exec_file(path, *arg, msh);
	free(path);
	return (ret);
}
