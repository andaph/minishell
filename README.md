# Minishell

This is a 42School project by dgoudet & alouis.


## A few ressources

### Git
* [Git pull without merging, how to override local files](https://forum.freecodecamp.org/t/git-pull-how-to-override-local-files-with-git-pull/13216)
* [A Hacker's Guide to Git](https://wildlyinaccurate.com/a-hackers-guide-to-git/)
* [Git From the Inside Out](https://maryrosecook.com/blog/post/git-from-the-inside-out)


### Makefile
* [Makefile : un résumé](http://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/Makefile.html)

### Unix system call
* [Man page execv](https://www.tutorialspoint.com/unix_system_calls/execve.htm)
* [Man page fork](https://www.tutorialspoint.com/unix_system_calls/fork.htm)

### Exit status
* [Exit status of a command shell ($?)](https://bash.cyberciti.biz/guide/The_exit_status_of_a_command)

### Useful links
* [Bash Reference manual](https://www.gnu.org/software/bash/manual/bash.html)
* [Redirections flows](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/40444-les-flux-de-redirection)
* [Redirections](https://catonmat.net/bash-one-liners-explained-part-three)
* [Pipes](http://www.zeitoun.net/articles/communication-par-tuyau/start)
