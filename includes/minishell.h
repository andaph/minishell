/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/28 16:01:20 by alouis            #+#    #+#             */
/*   Updated: 2021/02/06 08:58:50 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H
# include <sys/wait.h>
# include <unistd.h>
# include <stdlib.h>
# include <string.h>
# include <stdio.h>
# include <sys/types.h>
# include <dirent.h>
# include <signal.h>
# include <errno.h>
# include "libft.h"

typedef	enum			e_open_mode
{
	STDIN, STDOUT, TRUNC, APPEND, RDWR
}						t_mode;

typedef struct			s_fd
{
	int					fd;
	t_mode				md;
	char				*fl;
	struct s_fd			*next;
}						t_fd;

typedef struct			s_commands
{
	char				*cmd;
	char				**arg;
	int					end;
	t_fd				*in;
	t_fd				*out;
	struct s_commands	*next;
}						t_cmds;

typedef struct			s_msh
{
	char				**env;
	int					err;
	int					pipe;
	t_list				*cli;
	t_list				*tmp;
	t_cmds				*cmd;
	t_cmds				*cmd_tmp;
	char				*exp;
	void				*output;
	int					eop;
}						t_msh;

/*
**	---GLOBAL VARIABLES---
*/
int						g_sig;
int						g_quit;

/*
**	---MAIN---
*/
void					sighandler(int signum);

/*
**	---INIT & FREE---
*/
void					free_init_cli(t_msh *msh, char *ln);
t_cmds					*init_cmds();
void					free_cmds(t_cmds **cmd);
int						init_newfd(t_fd **fd, t_mode md, char *file);

/*
**	---PARSING---
*/
void					parsing(t_msh *msh, char *ln);
t_list					*split_cmdline(t_msh *msh, char *ln);
char					*create_maillon(t_msh *msh, char *content, size_t len);
char					*copy_specials(t_msh *msh, char **ln, char *tmp,
		char *spec);
void					copy_words(char **ln, char *tmp, int i);
void					copy_quotes(char **ln, char *tmp, int *i, char quote);
void					copy_escaped(char **ln, char *tmp);
void					copy_char(char **ln, char *tmp, int *i);
int						quote_is_escaped(char *str, char c);
int						find_snd_quote(char **tmp, char q);
int						split_cmds(t_cmds *cmd, char *str, int pipe, int *eop);
int						stock_cmd(t_cmds *cmd, char *str);
int						end_of_cmd(char *str, int pipe, int *eop);
void					ft_pipeadd_back(t_cmds **cmd);
int						ft_cleaning(t_msh *msh);
int						ft_check_quotes(char *tmp);
int						ft_expansion(t_msh *msh, char *cmd);
int						is_escaped(char *str, int i);
int						is_escaping(char *str);
int						ft_is_literal(char *cmd, int i);
int						even_unesc_of(char *str, char c, int i);
int						replace_with_value(t_msh *msh, char *str, int *i,
	int j);
char					*ft_get_value(char **env, char *str, int *i);
char					*put_error(char *varname);
int						replace_in_tab(t_msh *msh, int i);
void					get_end_arg(t_msh *msh, char *arg, int j, int len);
int						remove_quotes(char **str);
void					copy_after_double_quote(char **tmp, char **new, int *i);
void					copy_after_simple_quote(char **tmp, char **new, int *i);
void					copy_after_backslash(char **tmp, char **new, int *i);
void					ft_pipeadd_back(t_cmds **cmd);
int						redirection(t_cmds *cmd);
int						redirection_cmd(t_cmds *cmd, int *i);
int						redirection_arg(t_cmds *cmd, int *i);
int						remove_arg(t_cmds *cmd, int i);
int						replace_cmd(t_cmds *cmd, int *i);
int						replace_arg(t_cmds *cmd, int *i);
int						nbr_of_fd(t_fd *fd);
int						remove_all_quotes(char **str);
void					ft_copy_after_double_quote(char **tmp,
		char **new, int *i);
void					ft_copy_after_simple_quote(char **tmp,
		char **new, int *i);
int						remove_quotes_redirection(t_cmds *cmd);

/*
**	---COMMAND'S REDIRECTION---
*/
int						redirect_out(t_msh *msh);
int						redirect_last_cmd(t_msh *msh);
int						redirect_pipe(t_msh *msh);
void					redirect_cmd(t_msh *msh);
void					redirect_cmd2(t_msh *msh);
void					manage_builtin(t_msh *msh, int *ret);
void					manage_builtin2(t_msh *msh, int *ret);

/*
**	---UTILS---
*/
int						ft_cmdsize(t_cmds *cmd);
void					print_struct(t_cmds *cmd, char *str);
void					print_cmd(t_cmds *cmd, char *str);
void					sort_env(t_msh *msh);
void					sort_env_inlist(t_msh *msh);

/*
**	---CD & PWD---
*/
int						check_directory(char *dir);
void					up_and_chdir(char *buf, char *dir);
int						get_cd_nature(char *buf);
void					change_directory(char *buf, t_msh *msh);
void					print_cwd(t_msh *msh);
void					get_and_chdir(char *buf);
void					change_directory_3(char *buf, t_msh *msh);

/*
**	---ECHO---
*/
void					echo(t_msh *msh);
void					print_in_files(t_msh *msh);
void					print_args(char **arg, int fd);

/*
**	---EXEC---
*/
int						manage_exec(char *cmd, char ***arg, t_msh *msh);
int						manage_exec_2(char *cmd, char ***arg, t_msh *msh);
int						exec_file(char *path, char **arg, t_msh *msh);
int						add_cmd_to_arg(char *cmd, char ***arg);
void					exec_binary(char *buf, char **arg,
		int fd_out, t_msh *msh);
void					loop(char *buf, char **arg, t_fd *tmp, t_msh *msh);
int						add_cmd_to_arg(char *cmd, char ***arg);
char					*find_path(char *cmd, char **path_tab);
char					**create_path_tab(char **env);
int						redirect_and_exec(char *cmd, char ***arg,
		t_fd *tmp, t_msh *msh);
int						redirect_and_exec2(char *cmd, char ***arg,
	int fd, t_msh *msh);
int						exec_file2(char *cmd, char ***arg, int fd, t_msh *msh);
int						open_file(t_fd *rdc);

/*
**	---PIPE---
*/
int						manage_pipe(t_msh *msh);
int						send_fd_and_commands(t_msh *msh);
int						exec_commands(t_cmds *start, t_cmds *end, int fd,
	t_msh *msh);
int						create_pipe_and_fork(int (*pipefd)[2],
		pid_t *pid, t_cmds *start);
void					close_fd_in(int fd);

/*
**	---ENV---
*/
void					store_env(t_msh *msh);
void					write_env(t_msh *msh);
char					*ft_getenv(char *varname, char **env);
void					ft_setenv(char *varname, char *value, t_msh *msh);

/*
**	---EXPORT---
*/
void					ft_export(t_msh *msh);

/*
**	---UNSET---
*/
void					ft_unset(t_msh *msh);

/*
**	---ERR---
*/
void					set_err(int exit_status, t_msh *msh);
int						return_error(t_msh *msh);
void					error_127(t_msh *msh);

/*
**	---EXIT---
*/
void					ft_exit(t_msh *msh, void *other, char *msg);

#endif
